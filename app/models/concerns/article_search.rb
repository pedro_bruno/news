module ArticleSearch
	extend ActiveSupport::Concern

	included do 
		# Setting up ElasticSearch integration
		include Elasticsearch::Model 
		include Elasticsearch::Model::Callbacks 

		#Set Name of Index 
		index_name Rails.application.class.parent_name.underscore 
		document_type self.name.downcase

		#Mapping the document type
		settings index: { number_of_shards: 1 } do 
			mapping dynamic: false do				
				indexes :title, analyzer: 'portuguese' 
				indexes :author 
				indexes :text, analyzer: 'portuguese' 
				indexes :created_at, :type => 'date'
			end 
		end

		#Search Query
		def self.search(query)
	       __elasticsearch__.search(
	        {
	            query: {
	              multi_match: {
	                query: query,
	                fields: ['title^5', 'author', 'text']
	              }
	            },
	            highlight: {
	              pre_tags: ['<em>'],
	              post_tags: ['</em>'],
	              fields: {
	                title: {},
	                author: {},
	                text: {}
	              }
	            }
	          }
	        )
	      end

	    #Indexed fields
		def as_indexed_json(options = nil) 
			self.as_json( only: [ :title, :author, :text, :created_at ] ) 
		end
	end
end