class Article < ActiveRecord::Base
	include ArticleSearch

	validates_presence_of :title, :text, :author
end
