class Alert < ActiveRecord::Base

	validates_presence_of :name, :email, :keyword
end
