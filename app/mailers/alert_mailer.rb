class AlertMailer < ApplicationMailer
	default from: "news@news.com"
  	
  	def alert_keyword(alert, articles)
  		@alert = alert
  		@articles = articles  		

  		mail to: alert.email, 
  			 subject: "Notícias que podem interessar"
  	end
end
