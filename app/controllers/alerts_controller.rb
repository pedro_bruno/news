class AlertsController < ApplicationController
	before_action :set_alert, only: [:show]
	
	def new
	  @alert = Alert.new
	end	

	def show		
	end

	def create
	  @alert = Alert.new(alert_params)
	  @alert.last_news = Article.last.id
	  respond_to do |format|
	    if @alert.save
	      format.html { redirect_to @alert, notice: 'Alert was successfully created.' }
	    else
	      format.html { render :new }
	     end
	  end
  	end

	private

    def set_alert
      @alert = Alert.find(params[:id])
    end
    
    def alert_params
      params.require(:alert).permit(:name, :email, :keyword)
    end
end
