class SearchController < ApplicationController

	def search
	  if params[:q].nil?
	    @articles = []
	  else
	    @articles = Article.search(params[:q]).paginate(:page => params[:page], :per_page => 5)
	 end
	end

end
