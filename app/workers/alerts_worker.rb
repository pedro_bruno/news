class AlertsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  #Check if the words inputs by user are in the new News, if they are, will send a e-mail with the News.
  def perform
  	alerts = Alert.all
  	check  = false
  	alerts.each do |alert|
  		articles = Article.search(alert.keyword).results
	  		unless articles.nil? 
	  			articles.each do |article|
		  			unless (alert.last_news >= article.id.to_i)
		  				check = true		  			
		  			end
		  		end
		  		unless check == false
		  			AlertMailer.alert_keyword(alert, articles).deliver	
		  		end	
	  		end
	  	check = false  		
  	end
  	self.update   
  end

  #Update the last_news field for the users don't receive repetead e-mail.
  def update
  	updater = Article.all.last
  	Alert.update_all last_news: updater.id.to_i
  end
end