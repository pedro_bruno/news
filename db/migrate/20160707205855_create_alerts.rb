class CreateAlerts < ActiveRecord::Migration
  def change
    create_table :alerts do |t|
      t.string :name
      t.string :email
      t.string :keyword
      t.integer :last_news

      t.timestamps null: false
    end
  end
end
