desc "Search for keyword in ElasticSearch and if positive response, send a e-mail to user"
task word_alert_test: :environment do
  puts "Searching..."
  AlertsWorker.new.perform
  puts "done."
end