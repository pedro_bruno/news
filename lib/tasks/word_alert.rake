desc "Search for keyword in ElasticSearch and if positive response, send a e-mail to user"
task word_alert: :environment do
  puts "Searching..."
  AlertsWorker.perform_async
  puts "done."
end